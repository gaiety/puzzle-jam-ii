﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ChargeCounter : MonoBehaviour
{
    bool levelComplete = false;
    GameObject[] chargePoints;
    List<GameObject> chargedPoints = new List<GameObject>();
    Scene currentScene;

    void Start()
    {
        currentScene = SceneManager.GetActiveScene();
        if (chargePoints == null) chargePoints = GameObject.FindGameObjectsWithTag("Charge Point");
    }

    void Update()
    {
        if (levelComplete && Input.GetKeyDown("space")) LoadNextLevel();
    }

    public void AddChargedPoint(GameObject chargePoint)
    {
        chargedPoints.Add(chargePoint);
        DisplayCharges();
    }

    public void RemoveChargedPoint(GameObject chargePoint)
    {
        chargedPoints.Remove(chargePoint);
        DisplayCharges();
    }

    public bool IsLevelWon()
    {
        return levelComplete;
    }

    void DisplayCharges()
    {
        if (chargedPoints.Count >= chargePoints.Length) TriggerLevelWon();
    }

    void TriggerLevelWon()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        levelComplete = true;
    }

    void LoadNextLevel()
    {
        SceneManager.LoadScene(currentScene.buildIndex + 1);
    }
}

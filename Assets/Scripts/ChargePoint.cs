﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargePoint : MonoBehaviour
{
    public int chargesRequired = 1;
    public float particleMultiplier = 100.0f;
    public float particleStartingAmount = 20.0f;
    public Color chargedColor = Color.green;
    public Color unchargedColor = new Color(255, 182, 0);

    int charges = 0;
    bool charged = false;
    ChargeCounter chargeCounter;
    ParticleSystem ps;
    SpriteRenderer outerRing;
    SpriteRenderer middleRing;
    SpriteRenderer innerRing;
    AudioSource soundCharge1;
    AudioSource soundCharge2;
    AudioSource soundCharge3;
    AudioSource soundUncharge;

    void Start()
    {
        chargeCounter = GameObject.FindWithTag("Charge Counter").GetComponent<ChargeCounter>();
        ps = GetComponent<ParticleSystem>();
        outerRing = transform.Find("Rings/Outer").GetComponent<SpriteRenderer>();
        middleRing = transform.Find("Rings/Middle").GetComponent<SpriteRenderer>();
        innerRing = transform.Find("Rings/Inner").GetComponent<SpriteRenderer>();
        AudioSource[] sounds = GetComponents<AudioSource>();
        soundCharge1 = sounds[0];
        soundCharge2 = sounds[1];
        soundCharge3 = sounds[2];
        soundUncharge = sounds[3];

        VisualUpdate();
    }

    public void adjustCharges(int amount = 1)
    {
        charges += amount;

        if (charges >= chargesRequired)
        {
            if (!charged) chargeCounter.AddChargedPoint(gameObject);
            charged = true;
        }
        else
        {
            chargeCounter.RemoveChargedPoint(gameObject);
            charged = false;
        }

        if (amount < 0)
        {
            if (charges < chargesRequired) soundUncharge.Play();
        }
        else if (amount > 0)
        {
            if (charges == 1) soundCharge1.Play();
            if (charges == 2 && chargesRequired >= 2) soundCharge2.Play();
            if (charges == 3 && chargesRequired >= 3) soundCharge3.Play();
        }

        VisualUpdate();
    }

    void VisualUpdate()
    {
        var em = ps.emission;
        Color transparent = Color.white;
        transparent.a = 0;

        em.rateOverTime = (1.0f / chargesRequired * charges * particleMultiplier) + particleStartingAmount;
        outerRing.color = unchargedColor;
        middleRing.color = unchargedColor;
        innerRing.color = unchargedColor;
        if (chargesRequired < 3) innerRing.color = transparent;
        if (chargesRequired < 2) middleRing.color = transparent;
        if (charges >= 1) outerRing.color = chargedColor;
        if (charges >= 2 && chargesRequired >= 2) middleRing.color = chargedColor;
        if (charges >= 3 && chargesRequired >= 3) innerRing.color = chargedColor;

    }
}

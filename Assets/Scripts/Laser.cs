﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public bool flipDirection;
    public int power = 1;

    float maxDistance = 1000.0f;
    bool isHorizontal;
    LineRenderer laserLine;
    GameObject lastChargePoint;
    List<GameObject> boosters = new List<GameObject>();

    void Start()
    {
        laserLine = GetComponent<LineRenderer>();
        isHorizontal = GetComponent<Turret>().isHorizontal;
    }

    void Update()
    {
        DrawLaser(transform.position, isHorizontal, flipDirection);
    }

    int EffectivePower()
    {
        return power + boosters.Count;
    }

    void DrawLaser(Vector3 startPosition, bool drawOnYAxis, bool flipAxis, int laserIndex = 0)
    {
        RaycastHit2D hit = DrawRaycast(startPosition, drawOnYAxis, flipAxis);
        Vector3 endPosition;

        if (hit.collider) endPosition = hit.point;
        else if (drawOnYAxis) endPosition = new Vector3(startPosition.x, flipAxis ? startPosition.y - maxDistance : startPosition.y + maxDistance, 0);
        else endPosition = new Vector3(flipAxis ? startPosition.x - maxDistance : startPosition.x + maxDistance, startPosition.y, 0);

        laserLine.positionCount = laserIndex + 2;
        laserLine.SetPosition(laserIndex, startPosition);
        laserLine.SetPosition(laserIndex + 1, endPosition);

        if (hit.collider)
        {
            if (hit.collider.tag == "Mirror")
            {
                float mirrorZ = Mathf.Abs(hit.transform.eulerAngles.z);
                bool shouldFlip = flipAxis ? mirrorZ % 180 == 0 : mirrorZ % 180 > 0;
                Vector2 offset = GetDrawDirection(!drawOnYAxis, shouldFlip);
                DrawLaser(hit.point + offset, !drawOnYAxis, shouldFlip, laserLine.positionCount);
            }
            else if (hit.collider.tag == "Booster")
            {
                if (!boosters.Contains(hit.collider.gameObject)) boosters.Add(hit.collider.gameObject);
                Vector2 offset = GetDrawDirection(drawOnYAxis, flipAxis);
                DrawLaser(hit.point + offset, drawOnYAxis, flipAxis, laserLine.positionCount);
            }
            else if (hit.collider.tag == "Charge Point") HitChargePoint(hit);
            else UndoHitEffects();
        }
        else UndoHitEffects();
    }

    RaycastHit2D DrawRaycast(Vector3 startPosition, bool drawOnYAxis, bool flipAxis)
    {
        return Physics2D.Raycast(startPosition, GetDrawDirection(drawOnYAxis, flipAxis));
    }

    Vector2 GetDrawDirection(bool drawOnYAxis, bool flipAxis)
    {
        if (drawOnYAxis && flipAxis) return Vector2.down;
        else if (drawOnYAxis && !flipAxis) return Vector2.up;
        else if (!drawOnYAxis && flipAxis) return Vector2.left;
        return Vector2.right;
    }

    void HitChargePoint(RaycastHit2D hit)
    {
        if (hit.collider.gameObject != lastChargePoint) hit.collider.GetComponent<ChargePoint>().adjustCharges(EffectivePower());
        lastChargePoint = hit.collider.gameObject;
    }

    void UndoHitEffects()
    {
        if (lastChargePoint) lastChargePoint.GetComponent<ChargePoint>().adjustCharges(EffectivePower() * -1);
        lastChargePoint = null;
        boosters.Clear();
    }

    void OnDrawGizmosSelected()
    {
        float distance = 1.0f;
        isHorizontal = GetComponent<Turret>().isHorizontal;
        Vector3 offset = isHorizontal ? new Vector3(0, distance, 0) : new Vector3(distance, 0, 0);

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, flipDirection ? transform.position - offset : transform.position + offset);
    }
}

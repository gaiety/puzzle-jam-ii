﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    static MusicManager instance;
    bool gameStarted = false;
    AudioSource intro;
    AudioSource gameplay;

    void Start()
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        intro = audioSources[0];
        gameplay = audioSources[1];
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);    
        }
        else Destroy(gameObject);    
    }

    public void PlayNextSong(bool beginGameplayMusic)
    {
        if (beginGameplayMusic) gameStarted = true;
        if (gameStarted)
        {

            if (intro.isPlaying)
            {
                intro.Stop();
                gameplay.Play();
            }
        } else if (!intro.isPlaying) intro.Play();
    }
}

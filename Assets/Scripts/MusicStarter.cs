﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class MusicStarter : MonoBehaviour
{
    public bool beginGameplayMusic = false;
    int delayForSetup = 250;

    void Start()
    {
        DelayedSongRequest();
    }

    async void DelayedSongRequest()
    {
        await Task.Delay(delayForSetup);
        MusicManager m = GameObject.FindWithTag("MusicManager").GetComponent<MusicManager>();
        m.PlayNextSong(beginGameplayMusic);
    }
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadSceneName(string name)
    {
        SceneManager.LoadScene(name);
    }
}

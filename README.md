# Laser Link

[Published game on Itch.io](https://gaiety.itch.io/laser-link)

Made for [8 Bits to Infinity](http://8bitstoinfinity.com/)'s [Puzzle Jam II](https://itch.io/jam/puzzle-jam-ii).

![Cover](https://img.itch.zone/aW1nLzM3MDM1MDIuanBn/315x250%23c/93OsIM.jpg)

## Getting Started

1. Clone this repo locally
2. Follow [Thoughtbot's How to Git with Unity](https://thoughtbot.com/blog/how-to-git-with-unity).
3. Add folder to Unity Hub and start editing

